﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Login;

namespace WindowsFormsApp4.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
            Permissoes();
        }
         
        void Permissoes()
        {
            if (UserSession.UsuarioLogado.PermissaoAdm == false)
            {
                if (UserSession.UsuarioLogado.PermissaoCadastro == false)
                {
                    btnregistrar.Enabled = false;
                }

                if (UserSession.UsuarioLogado.PermissaoConsulta == false)
                {
                    button2.Enabled = false;
                }
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            frmRegistrar tela = new frmRegistrar();
            tela.Show();
            this.Close();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            frmPedido tela = new frmPedido();
            tela.Show();
            this.Close();
        }


        private void frmMenu01_Load(object sender, EventArgs e)
        {
            
        }

        private void btnOrcamento_Click(object sender, EventArgs e)
        {
            frmSolicitarorçamento tela = new frmSolicitarorçamento();
            tela.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmConsultar tela = new frmConsultar();
            tela.Show();
            this.Close();
        }
    }
}

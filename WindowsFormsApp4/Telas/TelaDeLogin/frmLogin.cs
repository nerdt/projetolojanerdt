﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Login;

namespace WindowsFormsApp4.Telas.TelaDeLogin
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            LoginBusiness business = new LoginBusiness();
            string user = txtUser.Text;
            string pass = txtPass.Text;
          
            LoginDTO usuario = business.Logar(user, pass);

            
            if (usuario != null)
            {
                UserSession.UsuarioLogado = usuario;

                frmMenu tela = new frmMenu();
                tela.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Nome de usuário ou senha incorretos.");
            }

        }

        private void lblRegistrar_Click(object sender, EventArgs e)
        {
            FrmCadastrarLogin tela = new FrmCadastrarLogin();
            tela.Show();
            this.Close();
        }
    }
}

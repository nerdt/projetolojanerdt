﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Login;

namespace WindowsFormsApp4.Telas.TelaDeLogin
{
    public partial class FrmCadastrarLogin : Form
    {
        public FrmCadastrarLogin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string usu = txtUsuario.Text;
                usu = usu.Trim();
                int qtdUsu = usu.Count();

                if (qtdUsu > 50)
                {
                    throw new Exception("O nome de usuário não pode passar de 20 caracteres");
                }
                else if (qtdUsu == 0)
                {
                    throw new Exception("O nome de usuário é obrigatório.");
                }

                string nome = txtNome.Text;
                nome = nome.Trim();
                int qtdNome = nome.Count();

                if (qtdNome > 100)
                {
                    throw new Exception("O nome não pode passar de 50 caracteres");
                }
                else if (qtdNome == 0)
                {
                    throw new Exception("O nome é obrigatório.");
                }

                string senha = txtSenha.Text;
                int qtdSenha = senha.Count();

                if (qtdSenha > 100)
                {
                    throw new Exception("A senha não pode passar de 45 caracteres");
                }
                else if (qtdSenha == 0)
                {
                    throw new Exception("A senha é obrigatória.");
                }

                LoginDTO dto = new LoginDTO();
                dto.Nome = txtNome.Text;
                dto.Email = txtEmail.Text;
                dto.Usuario = txtUsuario.Text;
                dto.Senha = txtSenha.Text;
                dto.PermissaoAdm = ckbAdm.Checked;
                dto.PermissaoCadastro = ckbCadastar.Checked;
                dto.PermissaoConsulta = ckbConsultar.Checked;

                LoginBusiness business = new LoginBusiness();
                business.Salvar(dto);

                MessageBox.Show("Cadastro efetuado com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (MySqlException ex)
            {
                if (ex.Number == 1062)
                {
                    MessageBox.Show("Esse nome de usuário já existe.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void label6_Click(object sender, EventArgs e)
        {
            frmLogin tela = new frmLogin();
            tela.Show();
            this.Close();
        }
    }
}

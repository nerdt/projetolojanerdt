﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Cliente;
using WindowsFormsApp4.DB.Orcamento;
using WindowsFormsApp4.Telas;

namespace WindowsFormsApp4
{
    public partial class frmSolicitarorçamento : Form
    {
        public frmSolicitarorçamento()
        {
            InitializeComponent();
            CarregarCombos();
            AutoCarregar();
        }

        public void CarregarGrid()
        {
            string nome = txtBuscar.Text;
            string a = string.Empty;

            ClienteBussines business = new ClienteBussines(); 
            List<ClienteDTO> dto = business.Consultar(nome);

            dgvListarCliente.AutoGenerateColumns = false;
            dgvListarCliente.DataSource = dto;
        }

        public void AutoCarregar()
        {
            ClienteBussines business = new ClienteBussines();
            List<ClienteDTO> dto = business.Listar();

            dgvListarCliente.AutoGenerateColumns = false;
            dgvListarCliente.DataSource = dto;
        }

        void CarregarCombos()
        {
            ClienteDTO dto = new ClienteDTO();
            ClienteBussines business = new ClienteBussines();
            List<ClienteDTO> lista = business.Listar();

            cboCliente.ValueMember = nameof(dto.Id);
            cboCliente.DisplayMember = nameof(dto.Nome);
            cboCliente.DataSource = lista;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void frmSolicitarorçamento_Load(object sender, EventArgs e)
        {

        }

        private void txtEmailO_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblemailO_Click(object sender, EventArgs e)
        {

        }

        private void lblNumerocasa_Click(object sender, EventArgs e)
        {

        }

        private void txtCidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnSolicitar_Click(object sender, EventArgs e)
        {
            ClienteDTO cliente = cboCliente.SelectedItem as ClienteDTO; 
            OrcamentoDTO dto = new OrcamentoDTO();
            dto.ClienteId = cliente.Id;
            dto.Valor = nudValor.Value;

            MessageBox.Show("Orçamento efetuado com sucesso!", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmVerOrcamentos tela = new frmVerOrcamentos();
            tela.Show();
            this.Close();
        }

        private void dgvListarCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                cboCliente.Text = dgvListarCliente[1, dgvListarCliente.CurrentRow.Index].Value.ToString();
            }
        }
    }
}

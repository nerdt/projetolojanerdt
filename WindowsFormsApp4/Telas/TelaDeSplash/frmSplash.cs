﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.Telas.TelaDeLogin;

namespace WindowsFormsApp4.Telas.TelaDeSplash
{
    public partial class frmSplash : Form
    {
        public frmSplash()
        {
            InitializeComponent();
            TimerProgress.Start();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void TimerProgress_Tick(object sender, EventArgs e)
        {
            progressBar1.Increment(1);
            if (progressBar1.Value == 100)
            {
                TimerProgress.Stop();
                frmLogin tela = new frmLogin();
                tela.Show();
                this.Hide();
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}

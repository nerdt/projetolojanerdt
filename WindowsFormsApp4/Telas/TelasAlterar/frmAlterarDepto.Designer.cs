﻿namespace WindowsFormsApp4.Telas.TelasAlterar
{
    partial class frmAlterarDepto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.btnRegistrardepto = new System.Windows.Forms.Button();
            this.lbllocal = new System.Windows.Forms.Label();
            this.txtlocal = new System.Windows.Forms.TextBox();
            this.txtNomedepto = new System.Windows.Forms.TextBox();
            this.lblNomeFor = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(261, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 17);
            this.label4.TabIndex = 98;
            this.label4.Text = "X";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // btnRegistrardepto
            // 
            this.btnRegistrardepto.Location = new System.Drawing.Point(54, 155);
            this.btnRegistrardepto.Name = "btnRegistrardepto";
            this.btnRegistrardepto.Size = new System.Drawing.Size(182, 47);
            this.btnRegistrardepto.TabIndex = 97;
            this.btnRegistrardepto.Text = "Alterar";
            this.btnRegistrardepto.UseVisualStyleBackColor = true;
            this.btnRegistrardepto.Click += new System.EventHandler(this.btnRegistrardepto_Click);
            // 
            // lbllocal
            // 
            this.lbllocal.AutoSize = true;
            this.lbllocal.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllocal.Location = new System.Drawing.Point(14, 59);
            this.lbllocal.Name = "lbllocal";
            this.lbllocal.Size = new System.Drawing.Size(56, 25);
            this.lbllocal.TabIndex = 96;
            this.lbllocal.Text = "Local";
            // 
            // txtlocal
            // 
            this.txtlocal.Location = new System.Drawing.Point(88, 59);
            this.txtlocal.Multiline = true;
            this.txtlocal.Name = "txtlocal";
            this.txtlocal.Size = new System.Drawing.Size(189, 73);
            this.txtlocal.TabIndex = 95;
            // 
            // txtNomedepto
            // 
            this.txtNomedepto.Location = new System.Drawing.Point(88, 33);
            this.txtNomedepto.Name = "txtNomedepto";
            this.txtNomedepto.Size = new System.Drawing.Size(189, 20);
            this.txtNomedepto.TabIndex = 94;
            this.txtNomedepto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomedepto_KeyPress);
            // 
            // lblNomeFor
            // 
            this.lblNomeFor.AutoSize = true;
            this.lblNomeFor.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeFor.Location = new System.Drawing.Point(8, 27);
            this.lblNomeFor.Name = "lblNomeFor";
            this.lblNomeFor.Size = new System.Drawing.Size(63, 25);
            this.lblNomeFor.TabIndex = 93;
            this.lblNomeFor.Text = "Nome";
            // 
            // frmAlterarDepto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 213);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnRegistrardepto);
            this.Controls.Add(this.lbllocal);
            this.Controls.Add(this.txtlocal);
            this.Controls.Add(this.txtNomedepto);
            this.Controls.Add(this.lblNomeFor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAlterarDepto";
            this.Text = "frmAlterarDepto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnRegistrardepto;
        private System.Windows.Forms.Label lbllocal;
        private System.Windows.Forms.TextBox txtlocal;
        private System.Windows.Forms.TextBox txtNomedepto;
        private System.Windows.Forms.Label lblNomeFor;
    }
}
﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Departamento;
using WindowsFormsApp4.DB.Funcionario;
using WindowsFormsApp4.Telas.Telas_Consultar;

namespace WindowsFormsApp4.Telas.TelasAlterar
{
    public partial class frmAlterarFuncionario : Form
    {
        public frmAlterarFuncionario()
        {
            InitializeComponent();
            CarregarCombos();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            ConsultarFuncionario tela = new ConsultarFuncionario();
            tela.Show();
            this.Close();
        }

        void CarregarCombos()
        {
            DB.Departamento.DeptoBussines business = new DB.Departamento.DeptoBussines();

            List<DeptoDTO> lista = business.Listar();

            cboDepartamento.ValueMember = nameof(DeptoDTO.Id);
            cboDepartamento.DisplayMember = nameof(DeptoDTO.Nome);
            cboDepartamento.DataSource = lista;
        }

        FuncionarioDTO dto;

        public void LoadScreen(FuncionarioDTO dto)
        {
            this.dto = dto;
            txtNomeF.Text = dto.Nome;
            txtEmailF.Text = dto.Email;
            txtCPF.Text = dto.CPF;
            mkbTelefone.Text = dto.Telefone;
            mkbEstado.Text = dto.Estado;
            txtCidade.Text = dto.Cidade;
            txtBairro.Text = dto.Bairro;
            cboDepartamento.SelectedItem = dto.IdDepartamento;
            txtRua.Text = dto.Rua;
            txtComplemento.Text = dto.COmplemento;
        }

        private void btnRegistrarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = txtNomeF.Text;
                nome = nome.Trim();
                int qtd = nome.Count();
                if (qtd > 50)
                {
                    throw new Exception("O campo 'Nome' não deve conter mais de 50 caracteres.");
                }
                else if (qtd == 0)
                {
                    throw new Exception("Nome inválido.");
                }

                string email = txtEmailF.Text;
                email = email.Trim();
                int qtdEmail = email.Count();

                if (qtdEmail > 150)
                {
                    throw new Exception("O E-mail não pode passar de 150 caracteres.");
                }
                else if (qtdEmail == 0)
                {
                    throw new Exception("Email inválido.");
                }

                string cidade = txtCidade.Text;
                cidade = cidade.Trim();
                int qtdCidade = cidade.Count();

                if (qtdCidade > 100)
                {
                    throw new Exception("O campo 'Cidade' não pode ter mais de 50 caracteres.");
                }
                else if (qtdCidade == 0)
                {
                    throw new Exception("Cidade inválida.");
                }

                string bairro = txtBairro.Text;
                bairro = bairro.Trim();
                int qtdBairro = bairro.Count();

                if (qtdBairro > 100)
                {
                    throw new Exception("O campo 'Bairro' não pode ter mais de 45 caracteres.");
                }
                else if (qtdBairro == 0)
                {
                    throw new Exception("Bairro inválido.");
                }

                string rua = txtRua.Text;
                rua = rua.Trim();
                int qtdRua = rua.Count();

                if (qtdRua > 100)
                {
                    throw new Exception("Rua inválida.");
                }

                string complemento = txtComplemento.Text;
                complemento = complemento.Trim();
                int qtdComp = complemento.Count();

                if (qtdComp > 100)
                {
                    throw new Exception("Rua inválida.");
                }

                DB.Departamento.DeptoDTO depto = cboDepartamento.SelectedItem as DB.Departamento.DeptoDTO;

                dto.Nome = txtNomeF.Text;
                dto.Email = txtEmailF.Text;

                dto.CPF = txtCPF.Text;
                dto.Telefone = mkbTelefone.Text;
                dto.IdDepartamento = depto.Id;
                dto.Cidade = txtCidade.Text;

                string estado = mkbEstado.Text;
                estado = estado.ToUpper();
                dto.Estado = estado;

                dto.Bairro = txtBairro.Text;
                dto.Rua = txtRua.Text;
                dto.COmplemento = txtComplemento.Text;

                DB.Funcionario.FuncionarioBussines bus = new DB.Funcionario.FuncionarioBussines();
                bus.Alterar(dto);
                MessageBox.Show("Funcionario alterado com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (MySqlException ex)
            {
                if (ex.Number == 1062)
                {
                    MessageBox.Show("Este funcionário já está cadastrado.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtNomeF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || e.KeyChar == (char)Keys.Back || char.IsWhiteSpace(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

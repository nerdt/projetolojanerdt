﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Departamento;
using WindowsFormsApp4.Telas.Telas_Consultar;

namespace WindowsFormsApp4.Telas.TelasAlterar
{
    public partial class frmAlterarDepto : Form
    {
        public frmAlterarDepto()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            ConsultarDepartamento tela = new ConsultarDepartamento();
            tela.Show();
            this.Close();
        }

        DeptoDTO dto;

        public void LoadScreen(DeptoDTO dto)
        {
            this.dto = dto;
            txtNomedepto.Text = dto.Nome;
            txtlocal.Text = dto.Local;
        }

        private void btnRegistrardepto_Click(object sender, EventArgs e)
        {
            try
            {
                DB.Departamento.DeptoDTO dto = new DB.Departamento.DeptoDTO();
                dto.Nome = txtNomedepto.Text;
                dto.Local = txtlocal.Text;

                DB.Departamento.DeptoBussines business = new DB.Departamento.DeptoBussines();
                business.Alterar(dto);

                MessageBox.Show("Departamento alterado com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void txtNomedepto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

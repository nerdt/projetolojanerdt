﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Peças;

namespace WindowsFormsApp4.Telas.TelasAlterar
{
    public partial class frmAlterarPeca : Form
    {
        public frmAlterarPeca()
        {
            InitializeComponent();
        }

        PeçasDTO dto;

        public void LoadScreen(PeçasDTO dto)
        {
            this.dto = dto;
            txtnomeP.Text = dto.Nome;
            nudValor.Value = dto.Valor;
            txtdescricao.Text = dto.Descricao;
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {

                string nome = txtnomeP.Text;
                nome = nome.Trim();
                int qtdNome = nome.Count();

                if (qtdNome > 50)
                {
                    throw new Exception("O nome da peça não pode passar de 50 caracteres.");
                }
                else if (qtdNome == 0)
                {
                    throw new Exception("Nome da peça é obrigatório");
                }

                string desc = txtdescricao.Text;
                desc = desc.Trim();
                int qtdDesc = desc.Count();

                if (qtdDesc > 200)
                {
                    throw new Exception("A descrição da peça não pode passar de 200 caracteres.");
                }
                else if (qtdDesc == 0)
                {
                    throw new Exception("Descrição da peça é obrigatório.");
                }


                dto.Nome = txtnomeP.Text;
                dto.Valor = nudValor.Value;
                dto.Descricao = txtdescricao.Text;

                DB.Peças.PeçasBussines bus = new DB.Peças.PeçasBussines();
                bus.Alterar(dto);
                MessageBox.Show("Sua Peça foi alterada com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmConsultarPecas tela = new frmConsultarPecas();
            tela.Show();
            this.Close();
        }

        private void txtnomeP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

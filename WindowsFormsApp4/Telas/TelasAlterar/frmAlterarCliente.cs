﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Cliente;

namespace WindowsFormsApp4.Telas.TelasAlterar
{
    public partial class frmAlterarCliente : Form
    {
        public frmAlterarCliente()
        {
            InitializeComponent();
            
        }

        private void label4_Click(object sender, EventArgs e)
        {
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
            this.Close();
        }

        ClienteDTO dto;

        public void LoadScreen(ClienteDTO dto)
        {
            this.dto = dto;
            txtNome.Text = dto.Nome;
            txtemail.Text = dto.Email;
            mkbTelefone.Text = dto.Telefone;
            mkbCPF.Text = dto.CPF;
            mkbEstado.Text = dto.Estado;
            txtCidade.Text = dto.Cidade;

        }

        private void btnRegistrarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = txtNome.Text;
                nome = nome.Trim();
                int qtd = nome.Count();
                if (qtd > 50)
                {
                    throw new Exception("O campo 'Nome' não deve conter mais de 50 caracteres.");
                }
                else if (qtd == 0)
                {
                    throw new Exception("Nome inválido.");
                }

                string email = txtemail.Text;
                email = email.Trim();
                int qtdEmail = email.Count();

                if (qtdEmail > 150)
                {
                    throw new Exception("O E-mail não pode passar de 150 caracteres.");
                }
                else if (qtdEmail == 0)
                {
                    throw new Exception("Email inválido.");
                }

                dto.Nome = txtNome.Text;
                dto.Telefone = mkbTelefone.Text;
                dto.CPF = mkbTelefone.Text;
                dto.Email = txtemail.Text;
                dto.Estado = mkbEstado.Text;
                dto.Cidade = txtCidade.Text;

                DB.Cliente.ClienteBussines bus = new DB.Cliente.ClienteBussines();
                bus.Alterar(dto);

                MessageBox.Show("Cliente alterado com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }
            catch (MySqlException ex)
            {
                if (ex.Number == 1062)
                {
                    MessageBox.Show("Este cliente já está cadastrado.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void frmAlterarCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || e.KeyChar == (char)Keys.Back || char.IsWhiteSpace(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

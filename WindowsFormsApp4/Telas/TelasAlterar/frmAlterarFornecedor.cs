﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Fornecedor;

namespace WindowsFormsApp4.Telas.TelasAlterar
{
    public partial class frmAlterarFornecedor : Form
    {
        public frmAlterarFornecedor()
        {
            InitializeComponent();
        }

        FornecedorDTO dto;

        public void LoadScreen(FornecedorDTO dto)
        {
            this.dto = dto;
            txtNomeFor.Text = dto.Nome;
            txtCPF_CNPJ.Text = dto.CNPJ;
            mkbEstado.Text = dto.Estado;
            txtCidadeF.Text = dto.Cidade;
        }

        private void label4_Click(object sender, EventArgs e)
        {
            ConsultarFornecedor tela = new ConsultarFornecedor();
            tela.Show();
            this.Close();
        }

        private void btnRegistrarfor_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = txtNomeFor.Text;
                nome = nome.Trim();
                int qtdNome = nome.Count();

                if (qtdNome > 50)
                {
                    throw new Exception("O campo 'Nome' não pode conter mais de 50 caracteres.");
                }
                else if (qtdNome == 0)
                {
                    throw new Exception("Nome inválido");
                }


                string cidade = txtCidadeF.Text;
                cidade = cidade.Trim();
                int qtdCidade = cidade.Count();

                if (qtdCidade > 100)
                {
                    throw new Exception("O campo 'Cidade' não pode conter mais de 45 caracteres.");
                }
                else if (qtdCidade == 0)
                {
                    throw new Exception("Cidade inválida.");
                }

                dto.Nome = txtNomeFor.Text;
                dto.CNPJ = txtCPF_CNPJ.Text;
                dto.Cidade = txtCidadeF.Text;
                dto.Estado = mkbEstado.Text;


                DB.Fornecedor.FornecedorBussines bus = new DB.Fornecedor.FornecedorBussines();
                bus.Alterar(dto);

                MessageBox.Show("Fornecedor alterado com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (MySqlException ex)
            {
                if (ex.Number == 1062)
                {
                    MessageBox.Show("Este fornecedor já está cadastrado.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtNomeFor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || e.KeyChar == (char)Keys.Back || char.IsWhiteSpace(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtCNPJ_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true || e.KeyChar == (char)Keys.Back || char.IsWhiteSpace(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

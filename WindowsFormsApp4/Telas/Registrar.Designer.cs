﻿namespace WindowsFormsApp4.Telas
{
    partial class frmRegistrar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistrar));
            this.lblRegistro = new System.Windows.Forms.Label();
            this.btnRGCliente = new System.Windows.Forms.Button();
            this.btnRGFuncionario = new System.Windows.Forms.Button();
            this.btnRGFornecedor = new System.Windows.Forms.Button();
            this.btnRGdepartamento = new System.Windows.Forms.Button();
            this.btnRGPeca = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRegistro
            // 
            this.lblRegistro.AutoSize = true;
            this.lblRegistro.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistro.Location = new System.Drawing.Point(30, 9);
            this.lblRegistro.Name = "lblRegistro";
            this.lblRegistro.Size = new System.Drawing.Size(92, 25);
            this.lblRegistro.TabIndex = 0;
            this.lblRegistro.Text = "Registrar";
            // 
            // btnRGCliente
            // 
            this.btnRGCliente.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRGCliente.Location = new System.Drawing.Point(17, 55);
            this.btnRGCliente.Name = "btnRGCliente";
            this.btnRGCliente.Size = new System.Drawing.Size(123, 46);
            this.btnRGCliente.TabIndex = 1;
            this.btnRGCliente.Text = "Registrar Cliente";
            this.btnRGCliente.UseVisualStyleBackColor = true;
            this.btnRGCliente.Click += new System.EventHandler(this.btnRGCliente_Click);
            // 
            // btnRGFuncionario
            // 
            this.btnRGFuncionario.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRGFuncionario.Location = new System.Drawing.Point(17, 159);
            this.btnRGFuncionario.Name = "btnRGFuncionario";
            this.btnRGFuncionario.Size = new System.Drawing.Size(123, 46);
            this.btnRGFuncionario.TabIndex = 2;
            this.btnRGFuncionario.Text = "Registrar Funcionario";
            this.btnRGFuncionario.UseVisualStyleBackColor = true;
            this.btnRGFuncionario.Click += new System.EventHandler(this.btnRGFuncionario_Click);
            // 
            // btnRGFornecedor
            // 
            this.btnRGFornecedor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRGFornecedor.Location = new System.Drawing.Point(17, 107);
            this.btnRGFornecedor.Name = "btnRGFornecedor";
            this.btnRGFornecedor.Size = new System.Drawing.Size(123, 46);
            this.btnRGFornecedor.TabIndex = 3;
            this.btnRGFornecedor.Text = "Registrar Fornecedor";
            this.btnRGFornecedor.UseVisualStyleBackColor = true;
            this.btnRGFornecedor.Click += new System.EventHandler(this.btnRGFornecedor_Click);
            // 
            // btnRGdepartamento
            // 
            this.btnRGdepartamento.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRGdepartamento.Location = new System.Drawing.Point(17, 211);
            this.btnRGdepartamento.Name = "btnRGdepartamento";
            this.btnRGdepartamento.Size = new System.Drawing.Size(123, 46);
            this.btnRGdepartamento.TabIndex = 4;
            this.btnRGdepartamento.Text = "Registrar Departamento";
            this.btnRGdepartamento.UseVisualStyleBackColor = true;
            this.btnRGdepartamento.Click += new System.EventHandler(this.btnRGdepartamento_Click);
            // 
            // btnRGPeca
            // 
            this.btnRGPeca.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRGPeca.Location = new System.Drawing.Point(17, 265);
            this.btnRGPeca.Name = "btnRGPeca";
            this.btnRGPeca.Size = new System.Drawing.Size(123, 46);
            this.btnRGPeca.TabIndex = 5;
            this.btnRGPeca.Text = "Registrar Peça";
            this.btnRGPeca.UseVisualStyleBackColor = true;
            this.btnRGPeca.Click += new System.EventHandler(this.btnRGPeca_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(172, 96);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(148, 144);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 77;
            this.pictureBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(320, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 17);
            this.label4.TabIndex = 78;
            this.label4.Text = "X";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // frmRegistrar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 326);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnRGPeca);
            this.Controls.Add(this.btnRGdepartamento);
            this.Controls.Add(this.btnRGFornecedor);
            this.Controls.Add(this.btnRGFuncionario);
            this.Controls.Add(this.btnRGCliente);
            this.Controls.Add(this.lblRegistro);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRegistrar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registrar";
            this.Load += new System.EventHandler(this.frmRegistrar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRegistro;
        private System.Windows.Forms.Button btnRGCliente;
        private System.Windows.Forms.Button btnRGFuncionario;
        private System.Windows.Forms.Button btnRGFornecedor;
        private System.Windows.Forms.Button btnRGdepartamento;
        private System.Windows.Forms.Button btnRGPeca;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
    }
}
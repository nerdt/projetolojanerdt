﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Pedido;

namespace WindowsFormsApp4.Telas
{
    public partial class frmConsultarPedido : Form
    {
        public frmConsultarPedido()
        {
            InitializeComponent();
            CarregarGrid();
        }

        void CarregarGrid()
        {
            PedidoDTO dto = new PedidoDTO();
            PedidoBusiness buss = new PedidoBusiness();
            List<PedidoDTO> lista = new List<PedidoDTO>();

            dgvPedido.AutoGenerateColumns = false;
            dgvPedido.DataSource = lista;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmPedido tela = new frmPedido();
            tela.Show();
            this.Close();
        }
    }
}

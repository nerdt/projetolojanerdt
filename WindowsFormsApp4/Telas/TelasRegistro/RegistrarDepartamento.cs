﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4.Telas.Telas_Registro
{
    public partial class FrmRegistrarDepartamento : Form
    {
        public FrmRegistrarDepartamento()
        {
            InitializeComponent();
        }

        private void btnRegistrardepto_Click(object sender, EventArgs e)
        {
            try
            {


                DB.Departamento.DeptoDTO dto = new DB.Departamento.DeptoDTO();
                dto.Nome = txtNomedepto.Text;
                dto.Local = txtlocal.Text;

                DB.Departamento.DeptoBussines business = new DB.Departamento.DeptoBussines();
                business.Salvar(dto);

                MessageBox.Show("Departamento registrado com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Error);
               
            }
          
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmRegistrar tela = new frmRegistrar();
            tela.Show();
            this.Close();
        }

        private void lbllocal_Click(object sender, EventArgs e)
        {

        }

        private void txtlocal_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNomedepto_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblNomeFor_Click(object sender, EventArgs e)
        {

        }

        private void txtNomedepto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

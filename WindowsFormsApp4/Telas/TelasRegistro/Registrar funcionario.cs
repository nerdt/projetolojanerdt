﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Departamento;
using WindowsFormsApp4.Telas;

namespace WindowsFormsApp4
{
    public partial class Registrar_funcionario : Form
    {
        public Registrar_funcionario()
        {
            InitializeComponent();
            CarregarCombos();
        }

        void CarregarCombos()
        {
            DB.Departamento.DeptoBussines business = new DB.Departamento.DeptoBussines();

            List<DeptoDTO> lista = business.Listar();

            cboDepartamento.ValueMember = nameof(DeptoDTO.Id);
            cboDepartamento.DisplayMember = nameof(DeptoDTO.Nome);
            cboDepartamento.DataSource = lista;
        }


        private void lblCidade_Click(object sender, EventArgs e)
        {

        }

        private void txtRua_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblRua_Click(object sender, EventArgs e)
        {

        }

        private void txtCidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCEP_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblCEP_Click(object sender, EventArgs e)
        {

        }

        private void cboEstadoC_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lblestado_Click(object sender, EventArgs e)
        {

        }

        private void txtSobrenomeF_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblSobrenomeF_Click(object sender, EventArgs e)
        {

        }

        private void txtNomeF_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblNomeF_Click(object sender, EventArgs e)
        {

        }

        private void txtNumero1F_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void Registrar_funcionario_Load(object sender, EventArgs e)
        {

        }

        private void btnRegistrarCliente_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = txtNomeF.Text;
                nome = nome.Trim();
                int qtd = nome.Count();
                if (qtd > 50)
                {
                    throw new Exception("O campo 'Nome' não deve conter mais de 50 caracteres.");
                }
                else if (qtd == 0)
                {
                    throw new Exception("Nome inválido.");
                }

                string email = txtEmailF.Text;
                email = email.Trim();
                int qtdEmail = email.Count();

                if (qtdEmail > 150)
                {
                    throw new Exception("O E-mail não pode passar de 150 caracteres.");
                }
                else if (qtdEmail == 0)
                {
                    throw new Exception("Email inválido.");
                }

                string cidade = txtCidade.Text;
                cidade = cidade.Trim();
                int qtdCidade = cidade.Count();

                if (qtdCidade > 100)
                {
                    throw new Exception("O campo 'Cidade' não pode ter mais de 50 caracteres.");
                }
                else if (qtdCidade == 0)
                {
                    throw new Exception("Cidade inválida.");
                }

                string bairro = txtBairro.Text;
                bairro = bairro.Trim();
                int qtdBairro = bairro.Count();

                if (qtdBairro > 100)
                {
                    throw new Exception("O campo 'Bairro' não pode ter mais de 45 caracteres.");
                }
                else if (qtdBairro == 0)
                {
                    throw new Exception("Bairro inválido.");
                }

                string rua = txtRua.Text;
                rua = rua.Trim();
                int qtdRua = rua.Count();

                if (qtdRua > 100)
                {
                    throw new Exception("Rua inválida.");
                }

                string complemento = txtComplemento.Text;
                complemento = complemento.Trim();
                int qtdComp = complemento.Count();

                if (qtdComp > 100)
                {
                    throw new Exception("Rua inválida.");
                }



                DB.Departamento.DeptoDTO depto = cboDepartamento.SelectedItem as DB.Departamento.DeptoDTO;
                DB.Funcionario.FuncionarioDTO dto = new DB.Funcionario.FuncionarioDTO();

                dto.Nome = txtNomeF.Text;
                dto.Email = txtEmailF.Text;

                dto.CPF = txtCPF.Text;
                dto.Telefone = mkbTelefone.Text;
                dto.IdDepartamento = depto.Id;
                dto.Cidade = txtCidade.Text;

                string estado = mkbEstado.Text;
                estado = estado.ToUpper();
                dto.Estado = estado;

                dto.Bairro = txtBairro.Text;
                dto.Rua = txtRua.Text;
                dto.COmplemento = txtComplemento.Text;

                DB.Funcionario.FuncionarioBussines bus = new DB.Funcionario.FuncionarioBussines();
                bus.Salvar(dto);
                MessageBox.Show("Funcionario cadastrado com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (MySqlException ex)
            {
                if (ex.Number == 1062)
                {
                    MessageBox.Show("Este funcionário já está cadastrado.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void cboDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmRegistrar tela = new frmRegistrar();
            tela.Show();
            this.Close();
        }

        private void txtComplemento_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtRua_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtBairro_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void mkbEstado_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtCPF_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void mkbTelefone_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void lblDepartamento_Click(object sender, EventArgs e)
        {

        }

        private void txtEmailF_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblemailF_Click(object sender, EventArgs e)
        {

        }

        private void lblNumero2F_Click(object sender, EventArgs e)
        {

        }

        private void lblCPFF_Click(object sender, EventArgs e)
        {

        }

        private void txtNomeF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || e.KeyChar == (char)Keys.Back || char.IsWhiteSpace(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

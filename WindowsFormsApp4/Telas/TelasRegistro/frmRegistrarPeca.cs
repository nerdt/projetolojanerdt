﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.Telas;

namespace WindowsFormsApp4
{
    public partial class frmRegistrarPeca : Form
    {
        public frmRegistrarPeca()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = txtnomeP.Text;
                nome = nome.Trim();
                int qtdNome = nome.Count();

                if (qtdNome > 50)
                {
                    throw new Exception("O nome da peça não pode passar de 50 caracteres.");
                }
                else if (qtdNome == 0)
                {
                    throw new Exception("Nome da peça é obrigatório");
                }

                string desc = txtdescricao.Text;
                desc = desc.Trim();
                int qtdDesc = desc.Count();

                if (qtdDesc > 200)
                {
                    throw new Exception("A descrição da peça não pode passar de 200 caracteres.");
                }
                else if (qtdDesc == 0)
                {
                    throw new Exception("Descrição da peça é obrigatório.");
                }



                DB.Peças.PeçasDTO dTO = new DB.Peças.PeçasDTO();

                dTO.Nome = txtnomeP.Text;
                dTO.Valor = nudValor.Value;
                dTO.Descricao = txtdescricao.Text;

                DB.Peças.PeçasBussines bus = new DB.Peças.PeçasBussines();
                bus.Salvar(dTO);
                MessageBox.Show("Sua Peça foi registrada com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void FrmRGpecas_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmRegistrar tela = new frmRegistrar();
            tela.Show();
            this.Close();
        }

        private void nudValor_ValueChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void lbldescricao_Click(object sender, EventArgs e)
        {

        }

        private void txtdescricao_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtnomeP_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

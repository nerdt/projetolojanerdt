﻿namespace WindowsFormsApp4
{
    partial class Registo_de_fornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNomeFor = new System.Windows.Forms.TextBox();
            this.lblNomeFor = new System.Windows.Forms.Label();
            this.lblCNPJ = new System.Windows.Forms.Label();
            this.txtCidadeF = new System.Windows.Forms.TextBox();
            this.lblCidadeF = new System.Windows.Forms.Label();
            this.lblestado = new System.Windows.Forms.Label();
            this.btnRegistrarfor = new System.Windows.Forms.Button();
            this.mkbEstado = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCPF_CNPJ = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // txtNomeFor
            // 
            this.txtNomeFor.Location = new System.Drawing.Point(103, 34);
            this.txtNomeFor.Name = "txtNomeFor";
            this.txtNomeFor.Size = new System.Drawing.Size(215, 20);
            this.txtNomeFor.TabIndex = 38;
            this.txtNomeFor.TextChanged += new System.EventHandler(this.txtNomeFor_TextChanged);
            this.txtNomeFor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomeFor_KeyPress);
            // 
            // lblNomeFor
            // 
            this.lblNomeFor.AutoSize = true;
            this.lblNomeFor.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeFor.Location = new System.Drawing.Point(29, 31);
            this.lblNomeFor.Name = "lblNomeFor";
            this.lblNomeFor.Size = new System.Drawing.Size(63, 25);
            this.lblNomeFor.TabIndex = 37;
            this.lblNomeFor.Text = "Nome";
            this.lblNomeFor.Click += new System.EventHandler(this.lblNomeFor_Click);
            // 
            // lblCNPJ
            // 
            this.lblCNPJ.AutoSize = true;
            this.lblCNPJ.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNPJ.Location = new System.Drawing.Point(36, 62);
            this.lblCNPJ.Name = "lblCNPJ";
            this.lblCNPJ.Size = new System.Drawing.Size(55, 25);
            this.lblCNPJ.TabIndex = 54;
            this.lblCNPJ.Text = "CNPJ";
            this.lblCNPJ.Click += new System.EventHandler(this.lblCNPJ_Click);
            // 
            // txtCidadeF
            // 
            this.txtCidadeF.Location = new System.Drawing.Point(103, 97);
            this.txtCidadeF.Name = "txtCidadeF";
            this.txtCidadeF.Size = new System.Drawing.Size(123, 20);
            this.txtCidadeF.TabIndex = 65;
            this.txtCidadeF.TextChanged += new System.EventHandler(this.txtCidadeF_TextChanged);
            // 
            // lblCidadeF
            // 
            this.lblCidadeF.AutoSize = true;
            this.lblCidadeF.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidadeF.Location = new System.Drawing.Point(21, 91);
            this.lblCidadeF.Name = "lblCidadeF";
            this.lblCidadeF.Size = new System.Drawing.Size(71, 25);
            this.lblCidadeF.TabIndex = 62;
            this.lblCidadeF.Text = "Cidade";
            this.lblCidadeF.Click += new System.EventHandler(this.lblCidadeF_Click);
            // 
            // lblestado
            // 
            this.lblestado.AutoSize = true;
            this.lblestado.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblestado.Location = new System.Drawing.Point(232, 62);
            this.lblestado.Name = "lblestado";
            this.lblestado.Size = new System.Drawing.Size(68, 25);
            this.lblestado.TabIndex = 61;
            this.lblestado.Text = "Estado";
            this.lblestado.Click += new System.EventHandler(this.lblestado_Click);
            // 
            // btnRegistrarfor
            // 
            this.btnRegistrarfor.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarfor.Location = new System.Drawing.Point(94, 139);
            this.btnRegistrarfor.Name = "btnRegistrarfor";
            this.btnRegistrarfor.Size = new System.Drawing.Size(157, 56);
            this.btnRegistrarfor.TabIndex = 88;
            this.btnRegistrarfor.Text = "Registrar";
            this.btnRegistrarfor.UseVisualStyleBackColor = true;
            this.btnRegistrarfor.Click += new System.EventHandler(this.btnRegistrarfor_Click);
            // 
            // mkbEstado
            // 
            this.mkbEstado.Location = new System.Drawing.Point(298, 67);
            this.mkbEstado.Mask = "AA";
            this.mkbEstado.Name = "mkbEstado";
            this.mkbEstado.Size = new System.Drawing.Size(20, 20);
            this.mkbEstado.TabIndex = 90;
            this.mkbEstado.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.mkbEstado_MaskInputRejected);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(319, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 17);
            this.label4.TabIndex = 91;
            this.label4.Text = "X";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtCPF_CNPJ
            // 
            this.txtCPF_CNPJ.Location = new System.Drawing.Point(103, 67);
            this.txtCPF_CNPJ.Mask = "99.999.999/9999-99";
            this.txtCPF_CNPJ.Name = "txtCPF_CNPJ";
            this.txtCPF_CNPJ.Size = new System.Drawing.Size(105, 20);
            this.txtCPF_CNPJ.TabIndex = 92;
            // 
            // Registo_de_fornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 224);
            this.Controls.Add(this.txtCPF_CNPJ);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.mkbEstado);
            this.Controls.Add(this.btnRegistrarfor);
            this.Controls.Add(this.txtCidadeF);
            this.Controls.Add(this.lblCidadeF);
            this.Controls.Add(this.lblestado);
            this.Controls.Add(this.lblCNPJ);
            this.Controls.Add(this.txtNomeFor);
            this.Controls.Add(this.lblNomeFor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Registo_de_fornecedor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registo de Fornecedor";
            this.Load += new System.EventHandler(this.Registo_de_fornecedor_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNomeFor;
        private System.Windows.Forms.Label lblNomeFor;
        private System.Windows.Forms.Label lblCNPJ;
        private System.Windows.Forms.TextBox txtCidadeF;
        private System.Windows.Forms.Label lblCidadeF;
        private System.Windows.Forms.Label lblestado;
        private System.Windows.Forms.Button btnRegistrarfor;
        private System.Windows.Forms.MaskedTextBox mkbEstado;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox txtCPF_CNPJ;
    }
}
﻿namespace WindowsFormsApp4.Telas.Telas_Registro
{
    partial class FrmRegistrarDepartamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNomedepto = new System.Windows.Forms.TextBox();
            this.lblNomeFor = new System.Windows.Forms.Label();
            this.lbllocal = new System.Windows.Forms.Label();
            this.txtlocal = new System.Windows.Forms.TextBox();
            this.btnRegistrardepto = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtNomedepto
            // 
            this.txtNomedepto.Location = new System.Drawing.Point(94, 38);
            this.txtNomedepto.Name = "txtNomedepto";
            this.txtNomedepto.Size = new System.Drawing.Size(189, 20);
            this.txtNomedepto.TabIndex = 40;
            this.txtNomedepto.TextChanged += new System.EventHandler(this.txtNomedepto_TextChanged);
            this.txtNomedepto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomedepto_KeyPress);
            // 
            // lblNomeFor
            // 
            this.lblNomeFor.AutoSize = true;
            this.lblNomeFor.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeFor.Location = new System.Drawing.Point(14, 32);
            this.lblNomeFor.Name = "lblNomeFor";
            this.lblNomeFor.Size = new System.Drawing.Size(63, 25);
            this.lblNomeFor.TabIndex = 39;
            this.lblNomeFor.Text = "Nome";
            this.lblNomeFor.Click += new System.EventHandler(this.lblNomeFor_Click);
            // 
            // lbllocal
            // 
            this.lbllocal.AutoSize = true;
            this.lbllocal.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllocal.Location = new System.Drawing.Point(20, 64);
            this.lbllocal.Name = "lbllocal";
            this.lbllocal.Size = new System.Drawing.Size(56, 25);
            this.lbllocal.TabIndex = 61;
            this.lbllocal.Text = "Local";
            this.lbllocal.Click += new System.EventHandler(this.lbllocal_Click);
            // 
            // txtlocal
            // 
            this.txtlocal.Location = new System.Drawing.Point(94, 64);
            this.txtlocal.Multiline = true;
            this.txtlocal.Name = "txtlocal";
            this.txtlocal.Size = new System.Drawing.Size(189, 73);
            this.txtlocal.TabIndex = 60;
            this.txtlocal.TextChanged += new System.EventHandler(this.txtlocal_TextChanged);
            // 
            // btnRegistrardepto
            // 
            this.btnRegistrardepto.Location = new System.Drawing.Point(60, 160);
            this.btnRegistrardepto.Name = "btnRegistrardepto";
            this.btnRegistrardepto.Size = new System.Drawing.Size(182, 47);
            this.btnRegistrardepto.TabIndex = 63;
            this.btnRegistrardepto.Text = "Registrar";
            this.btnRegistrardepto.UseVisualStyleBackColor = true;
            this.btnRegistrardepto.Click += new System.EventHandler(this.btnRegistrardepto_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(277, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 17);
            this.label4.TabIndex = 92;
            this.label4.Text = "X";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // FrmRegistrarDepartamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 233);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnRegistrardepto);
            this.Controls.Add(this.lbllocal);
            this.Controls.Add(this.txtlocal);
            this.Controls.Add(this.txtNomedepto);
            this.Controls.Add(this.lblNomeFor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmRegistrarDepartamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registrar Departamento";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNomedepto;
        private System.Windows.Forms.Label lblNomeFor;
        private System.Windows.Forms.Label lbllocal;
        private System.Windows.Forms.TextBox txtlocal;
        private System.Windows.Forms.Button btnRegistrardepto;
        private System.Windows.Forms.Label label4;
    }
}
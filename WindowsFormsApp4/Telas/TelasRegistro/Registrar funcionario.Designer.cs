﻿namespace WindowsFormsApp4
{
    partial class Registrar_funcionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Registrar_funcionario));
            this.txtEmailF = new System.Windows.Forms.TextBox();
            this.lblemailF = new System.Windows.Forms.Label();
            this.lblNumero2F = new System.Windows.Forms.Label();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.Label();
            this.lblestado = new System.Windows.Forms.Label();
            this.txtNomeF = new System.Windows.Forms.TextBox();
            this.lblCPFF = new System.Windows.Forms.Label();
            this.lblNomeF = new System.Windows.Forms.Label();
            this.cboDepartamento = new System.Windows.Forms.ComboBox();
            this.lblDepartamento = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnRegistrarCliente = new System.Windows.Forms.Button();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.mkbTelefone = new System.Windows.Forms.MaskedTextBox();
            this.mkbEstado = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRua = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // txtEmailF
            // 
            this.txtEmailF.Location = new System.Drawing.Point(139, 98);
            this.txtEmailF.Name = "txtEmailF";
            this.txtEmailF.Size = new System.Drawing.Size(259, 20);
            this.txtEmailF.TabIndex = 52;
            this.txtEmailF.TextChanged += new System.EventHandler(this.txtEmailF_TextChanged);
            // 
            // lblemailF
            // 
            this.lblemailF.AutoSize = true;
            this.lblemailF.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblemailF.Location = new System.Drawing.Point(9, 98);
            this.lblemailF.Name = "lblemailF";
            this.lblemailF.Size = new System.Drawing.Size(66, 25);
            this.lblemailF.TabIndex = 51;
            this.lblemailF.Text = "E-mail";
            this.lblemailF.Click += new System.EventHandler(this.lblemailF_Click);
            // 
            // lblNumero2F
            // 
            this.lblNumero2F.AutoSize = true;
            this.lblNumero2F.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero2F.Location = new System.Drawing.Point(9, 180);
            this.lblNumero2F.Name = "lblNumero2F";
            this.lblNumero2F.Size = new System.Drawing.Size(83, 25);
            this.lblNumero2F.TabIndex = 46;
            this.lblNumero2F.Text = "Telefone";
            this.lblNumero2F.Click += new System.EventHandler(this.lblNumero2F_Click);
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(182, 248);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(111, 20);
            this.txtCidade.TabIndex = 43;
            this.txtCidade.TextChanged += new System.EventHandler(this.txtCidade_TextChanged);
            this.txtCidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomeF_KeyPress);
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(113, 245);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(71, 25);
            this.lblCidade.TabIndex = 40;
            this.lblCidade.Text = "Cidade";
            this.lblCidade.Click += new System.EventHandler(this.lblCidade_Click);
            // 
            // lblestado
            // 
            this.lblestado.AutoSize = true;
            this.lblestado.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblestado.Location = new System.Drawing.Point(28, 245);
            this.lblestado.Name = "lblestado";
            this.lblestado.Size = new System.Drawing.Size(68, 25);
            this.lblestado.TabIndex = 39;
            this.lblestado.Text = "Estado";
            this.lblestado.Click += new System.EventHandler(this.lblestado_Click);
            // 
            // txtNomeF
            // 
            this.txtNomeF.Location = new System.Drawing.Point(139, 45);
            this.txtNomeF.Name = "txtNomeF";
            this.txtNomeF.Size = new System.Drawing.Size(259, 20);
            this.txtNomeF.TabIndex = 36;
            this.txtNomeF.TextChanged += new System.EventHandler(this.txtNomeF_TextChanged);
            this.txtNomeF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomeF_KeyPress);
            // 
            // lblCPFF
            // 
            this.lblCPFF.AutoSize = true;
            this.lblCPFF.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPFF.Location = new System.Drawing.Point(12, 134);
            this.lblCPFF.Name = "lblCPFF";
            this.lblCPFF.Size = new System.Drawing.Size(44, 25);
            this.lblCPFF.TabIndex = 35;
            this.lblCPFF.Text = "CPF";
            this.lblCPFF.Click += new System.EventHandler(this.lblCPFF_Click);
            // 
            // lblNomeF
            // 
            this.lblNomeF.AutoSize = true;
            this.lblNomeF.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeF.Location = new System.Drawing.Point(12, 45);
            this.lblNomeF.Name = "lblNomeF";
            this.lblNomeF.Size = new System.Drawing.Size(63, 25);
            this.lblNomeF.TabIndex = 33;
            this.lblNomeF.Text = "Nome";
            this.lblNomeF.Click += new System.EventHandler(this.lblNomeF_Click);
            // 
            // cboDepartamento
            // 
            this.cboDepartamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDepartamento.FormattingEnabled = true;
            this.cboDepartamento.Location = new System.Drawing.Point(167, 296);
            this.cboDepartamento.Name = "cboDepartamento";
            this.cboDepartamento.Size = new System.Drawing.Size(126, 21);
            this.cboDepartamento.TabIndex = 56;
            this.cboDepartamento.SelectedIndexChanged += new System.EventHandler(this.cboDepartamento_SelectedIndexChanged);
            // 
            // lblDepartamento
            // 
            this.lblDepartamento.AutoSize = true;
            this.lblDepartamento.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartamento.Location = new System.Drawing.Point(28, 292);
            this.lblDepartamento.Name = "lblDepartamento";
            this.lblDepartamento.Size = new System.Drawing.Size(133, 25);
            this.lblDepartamento.TabIndex = 57;
            this.lblDepartamento.Text = "Departamento";
            this.lblDepartamento.Click += new System.EventHandler(this.lblDepartamento_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(414, 45);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(157, 148);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 58;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnRegistrarCliente
            // 
            this.btnRegistrarCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRegistrarCliente.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarCliente.Location = new System.Drawing.Point(370, 338);
            this.btnRegistrarCliente.Name = "btnRegistrarCliente";
            this.btnRegistrarCliente.Size = new System.Drawing.Size(183, 64);
            this.btnRegistrarCliente.TabIndex = 59;
            this.btnRegistrarCliente.Text = "Registrar";
            this.btnRegistrarCliente.UseVisualStyleBackColor = true;
            this.btnRegistrarCliente.Click += new System.EventHandler(this.btnRegistrarCliente_Click);
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(139, 140);
            this.txtCPF.Mask = "000.000.000-00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(91, 20);
            this.txtCPF.TabIndex = 62;
            this.txtCPF.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtCPF_MaskInputRejected);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.pictureBox2.Location = new System.Drawing.Point(-4, 221);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(703, 10);
            this.pictureBox2.TabIndex = 64;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // mkbTelefone
            // 
            this.mkbTelefone.Location = new System.Drawing.Point(139, 186);
            this.mkbTelefone.Mask = "(99) 00000-0000";
            this.mkbTelefone.Name = "mkbTelefone";
            this.mkbTelefone.Size = new System.Drawing.Size(91, 20);
            this.mkbTelefone.TabIndex = 60;
            this.mkbTelefone.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.mkbTelefone_MaskInputRejected);
            // 
            // mkbEstado
            // 
            this.mkbEstado.Location = new System.Drawing.Point(92, 248);
            this.mkbEstado.Mask = "AA";
            this.mkbEstado.Name = "mkbEstado";
            this.mkbEstado.Size = new System.Drawing.Size(19, 20);
            this.mkbEstado.TabIndex = 66;
            this.mkbEstado.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.mkbEstado_MaskInputRejected);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(299, 245);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 25);
            this.label1.TabIndex = 67;
            this.label1.Text = "Bairro";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtBairro
            // 
            this.txtBairro.Location = new System.Drawing.Point(359, 248);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(212, 20);
            this.txtBairro.TabIndex = 68;
            this.txtBairro.TextChanged += new System.EventHandler(this.txtBairro_TextChanged);
            this.txtBairro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomeF_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(309, 292);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 25);
            this.label2.TabIndex = 69;
            this.label2.Text = "Rua";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtRua
            // 
            this.txtRua.Location = new System.Drawing.Point(359, 296);
            this.txtRua.Name = "txtRua";
            this.txtRua.Size = new System.Drawing.Size(212, 20);
            this.txtRua.TabIndex = 70;
            this.txtRua.TextChanged += new System.EventHandler(this.txtRua_TextChanged_1);
            this.txtRua.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomeF_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(29, 348);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 25);
            this.label3.TabIndex = 71;
            this.label3.Text = "Complemento";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // txtComplemento
            // 
            this.txtComplemento.Location = new System.Drawing.Point(166, 353);
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(127, 20);
            this.txtComplemento.TabIndex = 72;
            this.txtComplemento.TextChanged += new System.EventHandler(this.txtComplemento_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(559, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 17);
            this.label4.TabIndex = 92;
            this.label4.Text = "X";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // Registrar_funcionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 413);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtComplemento);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtRua);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBairro);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mkbEstado);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.txtCPF);
            this.Controls.Add(this.mkbTelefone);
            this.Controls.Add(this.btnRegistrarCliente);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblDepartamento);
            this.Controls.Add(this.cboDepartamento);
            this.Controls.Add(this.txtEmailF);
            this.Controls.Add(this.lblemailF);
            this.Controls.Add(this.lblNumero2F);
            this.Controls.Add(this.txtCidade);
            this.Controls.Add(this.lblCidade);
            this.Controls.Add(this.lblestado);
            this.Controls.Add(this.txtNomeF);
            this.Controls.Add(this.lblCPFF);
            this.Controls.Add(this.lblNomeF);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Registrar_funcionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registrar Funcionario";
            this.Load += new System.EventHandler(this.Registrar_funcionario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtEmailF;
        private System.Windows.Forms.Label lblemailF;
        private System.Windows.Forms.Label lblNumero2F;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.Label lblCidade;
        private System.Windows.Forms.Label lblestado;
        private System.Windows.Forms.TextBox txtNomeF;
        private System.Windows.Forms.Label lblCPFF;
        private System.Windows.Forms.Label lblNomeF;
        private System.Windows.Forms.ComboBox cboDepartamento;
        private System.Windows.Forms.Label lblDepartamento;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnRegistrarCliente;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.MaskedTextBox mkbTelefone;
        private System.Windows.Forms.MaskedTextBox mkbEstado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRua;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.Label label4;
    }
}
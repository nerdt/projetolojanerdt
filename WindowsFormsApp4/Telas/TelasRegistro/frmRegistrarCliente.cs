﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4.Telas
{
    public partial class frmCadastrarCliente : Form
    {
        public frmCadastrarCliente()
        {
            InitializeComponent();
        }

        private void frmCadastrarCliente_Load(object sender, EventArgs e)
        {

        }

        private void txtCEP_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnRegistrarCliente_Click(object sender, EventArgs e)
        {
           
            try
            {    
                string nome = txtNome.Text;
                nome = nome.Trim();
                int qtd = nome.Count();
                if (qtd > 50)
                {
                    throw new Exception("O campo 'Nome' não deve conter mais de 50 caracteres.");
                }
                else if (qtd == 0)
                {
                    throw new Exception("Nome inválido.");
                }

                string email = txtemail.Text;
                email = email.Trim();
                int qtdEmail = email.Count();

                if (qtdEmail > 150)
                {
                    throw new Exception("O E-mail não pode passar de 150 caracteres.");
                }
                else if (qtdEmail == 0)
                {
                    throw new Exception("Email inválido.");
                }

                string cidade = txtCidade.Text;
                cidade = cidade.Trim();
                int qtdCidade = cidade.Count();

                if (qtdCidade > 100)
                {
                    throw new Exception("A cidade não pode passar de 100 caracteres.");
                }
                else if (qtdCidade == 0)
                {
                    throw new Exception("Cidade inválida.");
                }

                DB.Cliente.ClienteDTO dto = new DB.Cliente.ClienteDTO();

                dto.Nome = txtNome.Text;
                dto.Telefone = mkbTelefone.Text;
                dto.CPF = mkbCPF.Text;
                dto.Email = txtemail.Text;
                dto.Estado = mkbEstado.Text;
                dto.Cidade = txtCidade.Text;

                DB.Cliente.ClienteBussines bus = new DB.Cliente.ClienteBussines();
                bus.Salvar(dto);

                MessageBox.Show("Cliente registrado com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }
            catch (MySqlException ex)
            {
                if (ex.Number == 1062)
                {
                    MessageBox.Show("Este cliente já está cadastrado.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }   
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Error);
               
            }
           
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmRegistrar tela = new frmRegistrar();
            tela.Show();
            this.Close();

        }

        private void mkbEstado_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void mkbTelefone_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtemail_TextChanged(object sender, EventArgs e)
        {

        }

        private void mkbCPF_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void lblemail_Click(object sender, EventArgs e)
        {

        }

        private void lblTitulo1_Click(object sender, EventArgs e)
        {

        }

        private void txtCidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblCidade_Click(object sender, EventArgs e)
        {

        }

        private void lblestado_Click(object sender, EventArgs e)
        {

        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void lblCPF_Click(object sender, EventArgs e)
        {

        }

        private void lblTelefone1_Click(object sender, EventArgs e)
        {

        }

        private void lblNomeCliente_Click(object sender, EventArgs e)
        {

        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsWhiteSpace(e.KeyChar) == true)
            {                          
                    e.Handled = false;   
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsWhiteSpace(e.KeyChar) == true || e.KeyChar == (char)Keys.Back)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}

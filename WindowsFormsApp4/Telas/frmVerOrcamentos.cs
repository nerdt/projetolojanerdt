﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Orcamento;

namespace WindowsFormsApp4.Telas
{
    public partial class frmVerOrcamentos : Form
    {
        public frmVerOrcamentos()
        {
            InitializeComponent();
            CarregarGrid();
        }

        void CarregarGrid()
        {
            OrcamentoDTO dto = new OrcamentoDTO();
            OrcamentoBusiness buss = new OrcamentoBusiness();
            List<OrcamentoDTO> lista = buss.Listar();

            dgvOrcamento.AutoGenerateColumns = false;
            dgvOrcamento.DataSource = lista;
        }

        private void dgvOrcamento_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmSolicitarorçamento tela = new frmSolicitarorçamento();
            tela.Show();
            this.Close();
        }
    }
}

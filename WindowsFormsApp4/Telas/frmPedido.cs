﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Orcamento;
using WindowsFormsApp4.DB.Peças;
using WindowsFormsApp4.DB.Pedido;

namespace WindowsFormsApp4.Telas
{
    public partial class frmPedido : Form
    {
        public frmPedido()
        {
            InitializeComponent();
            CarregarCboOrcamento();
            CarregarCboPeca();
        }

        void CarregarCboOrcamento()
        {
            OrcamentoDTO dto = new OrcamentoDTO();
            OrcamentoBusiness business = new OrcamentoBusiness();
            List<OrcamentoDTO> lista = business.Listar();

            cboOrcamento.ValueMember = nameof(dto.Id);
            cboOrcamento.DisplayMember = nameof(dto.ClienteId);
            cboOrcamento.DataSource = lista;
        } 

        void CarregarCboPeca()
        {
            PeçasDTO dto = new PeçasDTO();
            PeçasBussines buss = new PeçasBussines();
            List<PeçasDTO> lista = buss.Listar();

            cboPeca.ValueMember = nameof(dto.Id);
            cboPeca.DisplayMember = nameof(dto.Nome);
            cboPeca.DataSource = lista;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                PeçasDTO pecas = cboPeca.SelectedItem as PeçasDTO;
                OrcamentoDTO orcamento = cboOrcamento.SelectedItem as OrcamentoDTO;

                PedidoDTO dto = new PedidoDTO();
                dto.OrcamentoId = orcamento.Id;
                dto.PecaId = pecas.Id;
                dto.Data = mkbData.Text;

                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Pedido salvo com sucesso!", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Close();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            frmConsultarPedido tela = new frmConsultarPedido();
            tela.Show();
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4.Telas
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
        }

        private void btnCSCliente_Click(object sender, EventArgs e)
        {
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
            this.Close();
        }

        private void btnCSFuncionario_Click(object sender, EventArgs e)
        {
            Telas.Telas_Consultar.ConsultarFuncionario telas = new Telas.Telas_Consultar.ConsultarFuncionario();
            telas.Show();
            this.Close();
        }

        private void btnCSdepartamento_Click(object sender, EventArgs e)
        {
            Telas.Telas_Consultar.ConsultarDepartamento tela = new Telas.Telas_Consultar.ConsultarDepartamento();
            tela.Show();
            this.Close();
        }

        private void btnCSFornecedor_Click(object sender, EventArgs e)
        {
            ConsultarFornecedor tela = new ConsultarFornecedor();
            tela.Show();
            this.Close();
        }

        private void btnCSPeca_Click(object sender, EventArgs e)
        {
            frmConsultarPecas tela = new frmConsultarPecas();
            tela.Show();
            this.Close();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Close();
        }

        private void frmConsultar_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Peças;
using WindowsFormsApp4.Telas;
using WindowsFormsApp4.Telas.TelasAlterar;

namespace WindowsFormsApp4
{
    public partial class frmConsultarPecas : Form
    {
        public frmConsultarPecas()
        {
            InitializeComponent();
            AutoCarregar();
        }

        private void Frmconsultar_Load(object sender, EventArgs e)
        {

        }

        void AutoCarregar()
        {
            DB.Peças.PeçasBussines bus = new DB.Peças.PeçasBussines();
            List<DB.Peças.PeçasDTO> pecas = bus.Listar();
            gvPecas.AutoGenerateColumns = false;
            gvPecas.DataSource = pecas;
        }

        void CarregarDGV()
        {
            string nome = txtprocurar.Text;

            DB.Peças.PeçasBussines bus = new DB.Peças.PeçasBussines();
            List<DB.Peças.PeçasDTO> pecas = bus.Consultar(nome);
            gvPecas.AutoGenerateColumns = false;
            gvPecas.DataSource = pecas;
        }

        private void btnpesquisar_Click(object sender, EventArgs e)
        {
            CarregarDGV();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmConsultar tela = new frmConsultar();
            tela.Show();
            this.Close();
        }

        private void gvPecas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                PeçasDTO dto = gvPecas.Rows[e.RowIndex].DataBoundItem as PeçasDTO;
                frmAlterarPeca tela = new frmAlterarPeca();
                tela.LoadScreen(dto);
                tela.Show();
            }

            if (e.ColumnIndex == 5)
            {
                PeçasDTO dto = gvPecas.Rows[e.RowIndex].DataBoundItem as PeçasDTO;

                DialogResult resposta = MessageBox.Show("Quer mesmo apagar este registro?", "NerdT", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (resposta == DialogResult.Yes)
                {
                    PeçasBussines business = new PeçasBussines();
                    business.Remove(dto.Id);
                    MessageBox.Show("Registro removido com sucesso!", "NerdT", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                }
            }
        }
    }
}

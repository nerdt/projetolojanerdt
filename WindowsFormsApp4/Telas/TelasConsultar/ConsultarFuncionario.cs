﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Funcionario;
using WindowsFormsApp4.Telas.TelasAlterar;

namespace WindowsFormsApp4.Telas.Telas_Consultar
{
    public partial class ConsultarFuncionario : Form
    {
        public ConsultarFuncionario()
        {
            InitializeComponent();
            CarregarGrid();
        }

        void CarregarGrid()
        {
            DB.Funcionario.FuncionarioBussines bus = new DB.Funcionario.FuncionarioBussines();
            List<DB.Funcionario.FuncionarioDTO> fun = bus.Listar();

            gvFuncionario.AutoGenerateColumns = false;
            gvFuncionario.DataSource = fun;
        }

        void Buscar()
        {
            string nome = txtprocurar.Text;
            
            DB.Funcionario.FuncionarioBussines bus = new DB.Funcionario.FuncionarioBussines();
            List<DB.Funcionario.FuncionarioDTO> fun = bus.Consultar(nome);

            gvFuncionario.AutoGenerateColumns = false;
            gvFuncionario.DataSource = fun;
        }

        private void ConsultarCliente_Load(object sender, EventArgs e)
        {

        }

        private void btnpesquisar_Click(object sender, EventArgs e)
        {
            Buscar();
            
        }

        private void gvCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 11)
            {
                FuncionarioDTO dto = gvFuncionario.Rows[e.RowIndex].DataBoundItem as FuncionarioDTO;
                frmAlterarFuncionario tela = new frmAlterarFuncionario();
                tela.LoadScreen(dto);
                tela.Show();
            }

            if (e.ColumnIndex == 12)
            {
                FuncionarioDTO dto = gvFuncionario.Rows[e.RowIndex].DataBoundItem as FuncionarioDTO;

                DialogResult resposta = MessageBox.Show("Quer mesmo apagar este registro?", "NerdT", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (resposta == DialogResult.Yes)
                {
                    FuncionarioBussines business = new FuncionarioBussines();
                    business.Remover(dto.Id);
                    MessageBox.Show("Registro removido com sucesso!", "NerdT", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmConsultar tela = new frmConsultar();
            tela.Show();
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Fornecedor;
using WindowsFormsApp4.Telas.TelasAlterar;

namespace WindowsFormsApp4.Telas
{
    public partial class ConsultarFornecedor : Form
    {
        public ConsultarFornecedor()
        {
            InitializeComponent();
            AutoCarregar();
        }

        void AutoCarregar()
        {
            DB.Fornecedor.FornecedorBussines bus = new DB.Fornecedor.FornecedorBussines();
            List<DB.Fornecedor.FornecedorDTO> forn = bus.Listar();

            gvFornecedor.AutoGenerateColumns = false;
            gvFornecedor.DataSource = forn;
        }

        void CarregarGrid()
        {
            string nome = txtprocurar.Text;

            DB.Fornecedor.FornecedorBussines bus = new DB.Fornecedor.FornecedorBussines();
            List<DB.Fornecedor.FornecedorDTO> forn = bus.Consultar(nome);
            gvFornecedor.AutoGenerateColumns = false;
            gvFornecedor.DataSource = forn;
        }

        private void btnpesquisar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmConsultar tela = new frmConsultar();
            tela.Show();
            this.Close();
        }

        private void gvFornecedor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                FornecedorDTO dto = gvFornecedor.Rows[e.RowIndex].DataBoundItem as FornecedorDTO;
                frmAlterarFornecedor tela = new frmAlterarFornecedor();
                tela.LoadScreen(dto);
                tela.Show();
            }

            if (e.ColumnIndex == 6)
            {
                FornecedorDTO dto = gvFornecedor.Rows[e.RowIndex].DataBoundItem as FornecedorDTO;

                DialogResult resposta = MessageBox.Show("Quer mesmo apagar este registro?", "NerdT", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (resposta == DialogResult.Yes)
                {
                    FornecedorBussines business = new FornecedorBussines();
                    business.Remover(dto.Id);
                    MessageBox.Show("Registro removido com sucesso!", "NerdT", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                }
            }
        }
    }
}

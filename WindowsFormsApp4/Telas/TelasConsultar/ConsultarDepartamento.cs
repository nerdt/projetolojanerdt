﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.DB.Departamento;
using WindowsFormsApp4.Telas.TelasAlterar;

namespace WindowsFormsApp4.Telas.Telas_Consultar
{
    public partial class ConsultarDepartamento : Form
    {
        public ConsultarDepartamento()
        {
            InitializeComponent();
            AutoCarregar();
        }

        private void ConsultarDepartamento_Load(object sender, EventArgs e)
        {

        }

        void AutoCarregar()
        {
            DB.Departamento.DeptoBussines bus = new DB.Departamento.DeptoBussines();
            List<DB.Departamento.DeptoDTO> depto = bus.Listar();
            gvDepartamento.AutoGenerateColumns = false;
            gvDepartamento.DataSource = depto;
        }

        void Buscar()
        {
            string nome = txtprocurar.Text;

            DB.Departamento.DeptoBussines bus = new DB.Departamento.DeptoBussines();
            List<DB.Departamento.DeptoDTO> depto = bus.Consultar(nome);
            gvDepartamento.AutoGenerateColumns = false;
            gvDepartamento.DataSource = depto;
        }

        private void btnpesquisar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmConsultar tela = new frmConsultar();
            tela.Show();
            this.Close();
        }

        private void gvDepartamento_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                DeptoDTO dto = gvDepartamento.Rows[e.RowIndex].DataBoundItem as DeptoDTO;
                frmAlterarDepto tela = new frmAlterarDepto();
                tela.LoadScreen(dto);
                tela.Show();
            }

            if (e.ColumnIndex == 4)
            {
                DeptoDTO dto = gvDepartamento.Rows[e.RowIndex].DataBoundItem as DeptoDTO;

                DialogResult resposta = MessageBox.Show("Quer mesmo apagar este registro?", "NerdT", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (resposta == DialogResult.Yes)
                {
                    DeptoBussines business = new DeptoBussines();
                    business.Remover(dto.Id);
                    MessageBox.Show("Registro removido com sucesso!", "NerdT", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp4.Telas.TelasAlterar;

namespace WindowsFormsApp4.Telas
{
    public partial class ConsultarCliente : Form
    {
        public ConsultarCliente()
        {
            InitializeComponent();
            AutoCarregar();
        }

        void AutoCarregar()
        {
            DB.Cliente.ClienteBussines bus = new DB.Cliente.ClienteBussines();
            List<DB.Cliente.ClienteDTO> dto = bus.Listar();
            gvCliente.AutoGenerateColumns = false;
            gvCliente.DataSource = dto;
        }

        void CarregarGrid()
        {
            string nome = txtprocurar.Text;

            DB.Cliente.ClienteBussines bus = new DB.Cliente.ClienteBussines();
            List<DB.Cliente.ClienteDTO> dto = bus.Consultar(nome);
            gvCliente.AutoGenerateColumns = false;
            gvCliente.DataSource = dto;
        }

        private void ConsultarCliente_Load(object sender, EventArgs e)
        {
            
        }

        private void btnpesquisar_Click(object sender, EventArgs e)
        {
            CarregarGrid();

        }

        private void gvCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                DB.Cliente.ClienteDTO dto = gvCliente.Rows[e.RowIndex].DataBoundItem as DB.Cliente.ClienteDTO;
                frmAlterarCliente tela = new frmAlterarCliente();
                tela.LoadScreen(dto);
                tela.Show();
            }

            if (e.ColumnIndex == 8)
            {
                DB.Cliente.ClienteDTO dto = gvCliente.Rows[e.RowIndex].DataBoundItem as DB.Cliente.ClienteDTO;

                DialogResult resposta = MessageBox.Show("Quer mesmo apagar este registro?", "NerdT", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (resposta == DialogResult.Yes)
                {
                    DB.Cliente.ClienteBussines business = new DB.Cliente.ClienteBussines();
                    business.Remover(dto.Id);
                    MessageBox.Show("Registro removido com sucesso!", "NerdT", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frmConsultar tela = new frmConsultar();
            tela.Show();
            this.Close();
        }
    }
}

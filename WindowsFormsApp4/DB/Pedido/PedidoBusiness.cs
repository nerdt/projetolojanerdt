﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Pedido
{
    public class PedidoBusiness
    {
        public int Salvar(PedidoDTO dto)
        {
            if (dto.Data == "  /  /")
            {
                throw new Exception("O campo 'Nascimento' é obrigatório.");
            }

            PedidoDatabase database = new PedidoDatabase();
            return database.Salvar(dto);
        }

        public List<PedidoDTO> Listar()
        {
            PedidoDatabase database = new PedidoDatabase();
            return database.Listar();
        }
    }
}

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;

namespace WindowsFormsApp4.DB.Pedido
{
    public class PedidoDatabase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERT INTO tb_pedido(
                            id_peca,
                            dt_pedido,
                            id_orcamento) VALUES(
                            @id_peca,
                            @dt_pedido
                            @id_orcamento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_peca", dto.PecaId));
            parms.Add(new MySqlParameter("dt_pedido", dto.Data));
            parms.Add(new MySqlParameter("id_orcamento", dto.OrcamentoId));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<PedidoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoDTO> lista = new List<PedidoDTO>();
            while (reader.Read())
            {
                PedidoDTO dto = new PedidoDTO();
                dto.PecaId = reader.GetInt32("id_peca");
                dto.Data = reader.GetString("dt_pedido");
                dto.OrcamentoId = reader.GetInt32("id_orcamento");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}

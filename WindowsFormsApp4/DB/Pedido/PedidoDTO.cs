﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Pedido
{
    public class PedidoDTO
    {
        public int Id { get; set; }
        public int PecaId { get; set; }
        public string Data { get; set; }
        public int OrcamentoId { get; set; }
    }
}

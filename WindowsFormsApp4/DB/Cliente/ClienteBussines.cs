﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Cliente
{
    public class ClienteBussines
    {
        public int Salvar(ClienteDTO cliente)
        {
            //NOME

            if (cliente.Nome == string.Empty)
            {
                throw new Exception("O campo 'Cliente' é obrigatório.");
            }

            //EMAIL

            Validações.ValidarEmail email = new Validações.ValidarEmail();
            if (cliente.Email == string.Empty)
            {
                throw new Exception("O campo 'E-mail' é obrigatório.");
            }

            bool EMAIL = email.VerificarEmail(cliente.Email);
            if (EMAIL == false)
            {
                throw new Exception("E-mail inválido.");
            }

            //TELEFONE

            Validações.ValidarTelefone telefone = new Validações.ValidarTelefone();
            if (cliente.Telefone == string.Empty)
            {
                throw new Exception("O campo 'Telefone' é obrigatório.");
            }

            bool tell = telefone.VerificarTelefone(cliente.Telefone);
            if (tell == false)
            {
                throw new Exception("Telefone inválido.");
            }

            //CPF
            if (cliente.CPF == "   ,   ,   -")
            {
                throw new Exception("O campo 'CPF' é obrigatório.");
            }

            Validações.ValidarCPF cpf = new Validações.ValidarCPF();
            bool validouCpf = cpf.VerificaCpf(cliente.CPF);

            if (validouCpf == false)
            {
                throw new Exception("CPF inválido.");
            }


            //ESTADO

            Validações.ValidarUF estado = new Validações.ValidarUF();
            if (cliente.Estado == string.Empty)
            {
                throw new Exception("O campo 'Telefone' é obrigatório.");
            }

            bool UF = estado.VerificarUf(cliente.Estado);
            if (UF == false)
            {
                throw new Exception("O Estado informado é inválido.");
            }

            //CIDADE

            if (cliente.Cidade == string.Empty)
            {
                throw new Exception("O campo 'Cidade' é obrigatório.");
            }



            ClienteDataBase clientedb = new ClienteDataBase();
            int id = clientedb.Salvar(cliente);
            return id;
        }
      
        public void Remover(int idcliente)
        {
            ClienteDataBase clientedb = new ClienteDataBase();
            clientedb.Remover(idcliente);
        }
        public List<ClienteDTO> Listar()
        {
            ClienteDataBase clientedb = new ClienteDataBase();
            List<ClienteDTO> cliente = clientedb.Listar();
            return cliente;
        }

        public List<ClienteDTO> Consultar(string nome)
        {
            ClienteDataBase database = new ClienteDataBase();
            return database.Consultar(nome); 
        }

        public void Alterar(ClienteDTO cliente)
        {
            //NOME

            if (cliente.Nome == string.Empty)
            {
                throw new Exception("O campo 'Cliente' é obrigatório.");
            }

            //EMAIL

            Validações.ValidarEmail email = new Validações.ValidarEmail();
            if (cliente.Email == string.Empty)
            {
                throw new Exception("O campo 'E-mail' é obrigatório.");
            }

            bool EMAIL = email.VerificarEmail(cliente.Email);
            if (EMAIL == false)
            {
                throw new Exception("E-mail inválido.");
            }

            //CPF
            if (cliente.CPF == "   ,   ,   -")
            {
                throw new Exception("O campo 'CPF' é obrigatório.");
            }

            Validações.ValidarCPF cpf = new Validações.ValidarCPF();
            bool validouCpf = cpf.VerificaCpf(cliente.CPF);

            if (validouCpf == false)
            {
                throw new Exception("CPF inválido.");
            }


            //TELEFONE

            Validações.ValidarTelefone telefone = new Validações.ValidarTelefone();
            if (cliente.Telefone == string.Empty)
            {
                throw new Exception("O campo 'Telefone' é obrigatório.");
            }

            bool tell = telefone.VerificarTelefone(cliente.Telefone);
            if (tell == false)
            {
                throw new Exception("Telefone inválido.");
            }

        
            //ESTADO

            Validações.ValidarUF estado = new Validações.ValidarUF();
            if (cliente.Estado == string.Empty)
            {
                throw new Exception("O campo 'Telefone' é obrigatório.");
            }

            bool UF = estado.VerificarUf(cliente.Estado);
            if (UF == false)
            {
                throw new Exception("O Estado informado é inválido.");
            }

            //CIDADE

            if (cliente.Cidade == string.Empty)
            {
                throw new Exception("O campo 'Cidade' é obrigatório.");
            }

            ClienteDataBase clientedb = new ClienteDataBase();
            clientedb.Alterar(cliente);
        }
    }
}

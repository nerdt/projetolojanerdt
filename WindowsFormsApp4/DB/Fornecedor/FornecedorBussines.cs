﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Fornecedor
{
    class FornecedorBussines
    {
        public int Salvar(FornecedorDTO fornecedor)
        {
           FornecedorDataBase deptodb = new FornecedorDataBase();

            //NOME

           
            if (fornecedor.Nome == string.Empty)
            {
                throw new Exception("O campo 'Nome' não pode estar em branco.");

            }

            //CNPJ

            Validações.ValidarCNPJ_CPF cnpj = new Validações.ValidarCNPJ_CPF();
            if (fornecedor.CNPJ == string.Empty)
            {
                throw new Exception("O campo 'CNPJ' não pode estar em branco.");
            }

            bool validar = cnpj.VerificaCpfCnpj(fornecedor.CNPJ);
            if (validar == false)
            {
                throw new Exception("CNPJ inválido.");
            }

            //ESTADO

            Validações.ValidarUF UF = new Validações.ValidarUF();
            if (fornecedor.Estado == string.Empty)
            {
                throw new Exception("O campo 'Estado' não pode estar em branco.");

            }

            bool validarUF = UF.VerificarUf(fornecedor.Estado);
            if (validarUF == false)
            {
                throw new Exception("Estado inválido.");

            }

            //CIDADE

            if (fornecedor.Cidade == string.Empty)
            {
                throw new Exception("O campo 'Cidade' não pode estar em branco.");

            }

            int id = deptodb.Salvar(fornecedor);
            return id;
        }

        public void Alterar(FornecedorDTO fornecedor)
        {
            FornecedorDataBase database = new FornecedorDataBase();

            //NOME


            if (fornecedor.Nome == string.Empty)
            {
                throw new Exception("O campo 'Nome' não pode estar em branco.");

            }

            //CNPJ

            Validações.ValidarCNPJ cnpj = new Validações.ValidarCNPJ();
            if (fornecedor.CNPJ == string.Empty)
            {
                throw new Exception("O campo 'CNPJ' não pode estar em branco.");
            }

            bool validar = cnpj.VerificaCNPJ(fornecedor.CNPJ);
            if (validar == false)
            {
                throw new Exception("CNPJ inválido.");
            }

            //ESTADO

            Validações.ValidarUF UF = new Validações.ValidarUF();
            if (fornecedor.Estado == string.Empty)
            {
                throw new Exception("O campo 'Estado' não pode estar em branco.");

            }

            bool validarUF = UF.VerificarUf(fornecedor.Estado);
            if (validarUF == false)
            {
                throw new Exception("Estado inválido.");

            }

            //CIDADE

            if (fornecedor.Cidade == string.Empty)
            {
                throw new Exception("O campo 'Cidade' não pode estar em branco.");

            }

            database.Alterar(fornecedor);
        }
    
        public void Remover(int idFornecedor)
        {
            FornecedorDataBase deptodb = new FornecedorDataBase();
            deptodb.Remover(idFornecedor);
        }
        public List<FornecedorDTO> Listar()
        {
            FornecedorDataBase deptodb = new FornecedorDataBase();
            return deptodb.Listar();
            
        }

        public List<FornecedorDTO> Consultar(string nome)
        {
            FornecedorDataBase databse = new FornecedorDataBase();
            return databse.Consultar(nome);
        }


    }
}

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;

namespace WindowsFormsApp4.DB.Fornecedor
{
    class FornecedorDataBase
    {
        public int Salvar(FornecedorDTO forn)
        {
            string script = @"INSERT INTO tb_fornecedor(
                 nm_fornecedor,
	             ds_cnpj,
	             nm_cidade,
                 nm_estado) VALUES(
                 @nm_fornecedor,
                 @ds_cnpj,
	             @nm_cidade,
                 @nm_estado)";
                
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", forn.Nome));
            parms.Add(new MySqlParameter("ds_cnpj", forn.CNPJ));
            parms.Add(new MySqlParameter("nm_cidade", forn.Cidade));
            parms.Add(new MySqlParameter("nm_estado", forn.Estado));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(FornecedorDTO dto)
        {
            string script = @"UPDATE tb_fornecedor SET
                           nm_fornecedor = @nm_fornecedor,
                           ds_cnpj = @ds_cnpj,
                           nm_cidade = @nm_cidade,
                           nm_estado = @nm_estado WHERE
                           id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", dto.Id));
            parms.Add(new MySqlParameter("nm_fornecedor", dto.Nome));
            parms.Add(new MySqlParameter("ds_cnpj", dto.CNPJ));
            parms.Add(new MySqlParameter("nm_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("nm_estado", dto.Estado));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }


        public void Remover(int idforn)
        {
            string script = @"DELETE FROM tb_fornecedor WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", idforn));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<FornecedorDTO> Listar()
        {
            string script = @"SELECT * FROM tb_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> endereco = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO DTO = new FornecedorDTO();
                DTO.Id = reader.GetInt32("id_fornecedor");
                DTO.Nome = reader.GetString("nm_fornecedor");
                DTO.CNPJ = reader.GetString("ds_cnpj");
                DTO.Cidade = reader.GetString("nm_cidade");
                DTO.Estado = reader.GetString("nm_estado");

                endereco.Add(DTO);
            }
            reader.Close();
            return endereco;
        }

        public List<FornecedorDTO> Consultar(string nome)
        {
            string script = @"SELECT * FROM tb_fornecedor WHERE nm_fornecedor LIKE @nm_forncedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> endereco = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO DTO = new FornecedorDTO();
                DTO.Id = reader.GetInt32("id_fornecedor");
                DTO.Nome = reader.GetString("nm_fornecedor");
                DTO.CNPJ = reader.GetString("ds_cnpj");
                DTO.Cidade = reader.GetString("nm_cidade");
                DTO.Estado = reader.GetString("nm_estado");

                endereco.Add(DTO);
            }
            reader.Close();
            return endereco;
        }
    }
}

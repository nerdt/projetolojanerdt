﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Peças
{
    class PeçasBussines
    {
        public int Salvar(PeçasDTO pecas)
        {
            PeçasDataBase deptodb = new PeçasDataBase();

            //PEÇA

            if (pecas.Nome == string.Empty)
            {
                throw new Exception("O campo 'Nome' não pode estar em branco.");
            }

            //VALOR

            if (pecas.Valor == 0)
            {
                throw new Exception("O campo 'Nome' não pode ser zero.");

            }

            //DESCRIÇÃO

            if (pecas.Descricao == string.Empty)
            {
                throw new Exception("O campo 'Descrição' não pode estar em branco.");

            }

            int id = deptodb.Salvar(pecas);
            return id;
        }
        public void Alterar(PeçasDTO pecas)
        {
            PeçasDataBase deptodb = new PeçasDataBase();
            //PEÇA

            if (pecas.Nome == string.Empty)
            {
                throw new Exception("O campo 'Nome' não pode estar em branco.");
            }

            //VALOR

            if (pecas.Valor == 0)
            {
                throw new Exception("O campo 'Nome' não pode ser zero.");

            }

            //DESCRIÇÃO

            if (pecas.Descricao == string.Empty)
            {
                throw new Exception("O campo 'Descrição' não pode estar em branco.");

            }

            deptodb.Alterar(pecas);
        }
        public void Remove(int idcliente)
        {
            PeçasDataBase deptodb = new PeçasDataBase();
            deptodb.Remover(idcliente);
        }
        public List<PeçasDTO> Listar()
        {
            PeçasDataBase deptodb = new PeçasDataBase();
            List<PeçasDTO> depto = deptodb.Listar();
            return depto;
        }

        public List<PeçasDTO> Consultar(string nome)
        {
            PeçasDataBase database = new PeçasDataBase();
            return database.Consultar(nome);
        }

    }
}

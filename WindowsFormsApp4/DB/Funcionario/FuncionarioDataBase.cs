﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;

namespace WindowsFormsApp4.DB.Funcionario
{
    class FuncionarioDataBase
    {
        public int Salvar(FuncionarioDTO func)
        {
            string script = @"INSERT INTO tb_funcionario(
                  nm_funcionario,
	              ds_telefone,
	              ds_email,
	              ds_cpf,
	              id_depto,
	              nm_cidade,
                  nm_estado,
                  nm_bairro,
                  nm_rua,
                  ds_complemento) VALUES(
                  @nm_funcionario,
	              @ds_telefone,
	              @ds_email,
	              @ds_cpf,
	              @id_depto,
	              @nm_cidade,
                  @nm_estado,
                  @nm_bairro,
                  @nm_rua,
                  @ds_complemento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", func.Nome));
            parms.Add(new MySqlParameter("ds_telefone", func.Telefone));
            parms.Add(new MySqlParameter("ds_email", func.Email));
            parms.Add(new MySqlParameter("ds_cpf", func.CPF));
            parms.Add(new MySqlParameter("id_depto", func.IdDepartamento));
            parms.Add(new MySqlParameter("nm_cidade", func.Cidade));
            parms.Add(new MySqlParameter("nm_estado", func.Estado));
            parms.Add(new MySqlParameter("nm_bairro", func.Bairro));
            parms.Add(new MySqlParameter("nm_rua", func.Rua));
            parms.Add(new MySqlParameter("ds_complemento", func.COmplemento));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(FuncionarioDTO func)
        {
            string script = @"UPDATE tb_funcionario SET 
                            nm_funcionario = @nm_funcionario,
	                        ds_telefone = @ds_telefone,
	                        ds_email = @ds_email,
	                        ds_cpf = @ds_cpf,
	                        id_depto = @id_depto,
                            nm_cidade = @nm_cidade,
                            nm_estado = @nm_estado,
                            nm_bairro = @nm_bairro,
                            nm_rua = @nm_rua,
	                        ds_complemento = @ds_complemento WHERE 
                            id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", func.Id));
            parms.Add(new MySqlParameter("nm_funcionario", func.Nome));
            parms.Add(new MySqlParameter("ds_telefone", func.Telefone));
            parms.Add(new MySqlParameter("ds_email", func.Email));
            parms.Add(new MySqlParameter("ds_cpf", func.CPF));
            parms.Add(new MySqlParameter("id_depto", func.IdDepartamento));
            parms.Add(new MySqlParameter("nm_cidade", func.Cidade));
            parms.Add(new MySqlParameter("nm_estado", func.Estado));
            parms.Add(new MySqlParameter("nm_bairro", func.Bairro));
            parms.Add(new MySqlParameter("nm_rua", func.Rua));
            parms.Add(new MySqlParameter("ds_complemento", func.COmplemento));


            Database db = new Database();
            db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int idfunc)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", idfunc));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<FuncionarioDTO> Listar()
        {
            string script = @"SELECT * FROM tb_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> endereco = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO DTO = new FuncionarioDTO();
                DTO.Id = reader.GetInt32("id_funcionario");
                DTO.Nome = reader.GetString("nm_funcionario");
                DTO.Telefone = reader.GetString("ds_telefone");
                DTO.Email = reader.GetString("ds_email");
                DTO.CPF = reader.GetString("ds_cpf");
                DTO.IdDepartamento = reader.GetInt32("id_depto");
                DTO.Cidade = reader.GetString("nm_cidade");
                DTO.Estado = reader.GetString("nm_estado");
                DTO.Bairro = reader.GetString("nm_bairro");
                DTO.Rua = reader.GetString("nm_rua");
                DTO.COmplemento = reader.GetString("ds_complemento");

                endereco.Add(DTO);
            }
            reader.Close();
            return endereco;
        }

        public List<FuncionarioDTO> Consultar(string nome)
        {
            string script = @"SELECT * FROM tb_funcionario WHERE nm_funcionario LIKE @nm_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> endereco = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO DTO = new FuncionarioDTO();
                DTO.Id = reader.GetInt32("id_funcionario");
                DTO.Nome = reader.GetString("nm_funcionario");
                DTO.Telefone = reader.GetString("ds_telefone");
                DTO.Email = reader.GetString("ds_email");
                DTO.CPF = reader.GetString("ds_cpf");
                DTO.IdDepartamento = reader.GetInt32("id_depto");
                DTO.Cidade = reader.GetString("nm_cidade");
                DTO.Estado = reader.GetString("nm_estado");
                DTO.Bairro = reader.GetString("nm_bairro");
                DTO.Rua = reader.GetString("nm_rua");
                DTO.COmplemento = reader.GetString("ds_complemento");

                endereco.Add(DTO);
            }
            reader.Close();
            return endereco;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Funcionario
{
    class FuncionarioBussines
    {
        public int Salvar(FuncionarioDTO funcionario)
        {
            FuncionarioDataBase deptodb = new FuncionarioDataBase();

            // CPF

            Validações.ValidarCPF cpf = new Validações.ValidarCPF();
            if (funcionario.CPF == string.Empty)
            {
                throw new Exception("O campo 'CPF' é obrigatório.");
            }

            bool CPF = cpf.VerificaCpf(funcionario.CPF);
            if (CPF == false)
            {
                throw new Exception("CPF inválido.");
            }

            //ESTADO

            Validações.ValidarUF estado = new Validações.ValidarUF();
            if (funcionario.Estado == string.Empty)
            {
                throw new Exception("O campo 'Estado' é obrigatório.");
            }

            bool UF = estado.VerificarUf(funcionario.Estado);
            if (UF == false)
            {
                throw new Exception("O Estado informado é inválido.");
            }

            //EMAIL

            Validações.ValidarEmail email = new Validações.ValidarEmail();
            if (funcionario.Email == string.Empty)
            {
                throw new Exception("O campo 'E-mail' é obrigatório.");
            }

            bool EMAIL = email.VerificarEmail(funcionario.Email);
            if (EMAIL == false)
            {
                throw new Exception("E-mail inválido.");
            }

            //TELEFONE

            Validações.ValidarTelefone telefone = new Validações.ValidarTelefone();
            if (funcionario.Telefone == string.Empty)
            {
                throw new Exception("O campo 'Telefone' é obrigatório.");
            }

            bool tell = telefone.VerificarTelefone(funcionario.Telefone);
            if (tell == false)
            {
                throw new Exception("Telefone inválido.");
            }

            //NOME

            if (funcionario.Nome == string.Empty)
            {
                throw new Exception("O campo 'Nome' é obrigatório.");
            }

            //CIDADE

            if (funcionario.Cidade == string.Empty)
            {
                throw new Exception("O campo 'Cidade' é obrigatório.");
            }

            //BAIRRO

            if (funcionario.Bairro == string.Empty)
            {
                throw new Exception("O campo 'Bairro' é obrigatório.");
            }

            //RUA

            if (funcionario.Rua == string.Empty)
            {
                throw new Exception("O campo 'Rua' é obrigatório.");
            }

            int id = deptodb.Salvar(funcionario);
            return id;
        }
        public void Alterar(FuncionarioDTO funcionario)
        {
            FuncionarioDataBase deptodb = new FuncionarioDataBase();
            // CPF

            Validações.ValidarCPF cpf = new Validações.ValidarCPF();
            if (funcionario.CPF == string.Empty)
            {
                throw new Exception("O campo 'CPF' é obrigatório.");
            }

            bool CPF = cpf.VerificaCpf(funcionario.CPF);
            if (CPF == false)
            {
                throw new Exception("CPF inválido.");
            }

            //ESTADO

            Validações.ValidarUF estado = new Validações.ValidarUF();
            if (funcionario.Estado == string.Empty)
            {
                throw new Exception("O campo 'Estado' é obrigatório.");
            }

            bool UF = estado.VerificarUf(funcionario.Estado);
            if (UF == false)
            {
                throw new Exception("O Estado informado é inválido.");
            }

            //EMAIL

            Validações.ValidarEmail email = new Validações.ValidarEmail();
            if (funcionario.Email == string.Empty)
            {
                throw new Exception("O campo 'E-mail' é obrigatório.");
            }

            bool EMAIL = email.VerificarEmail(funcionario.Email);
            if (EMAIL == false)
            {
                throw new Exception("E-mail inválido.");
            }

            //TELEFONE

            Validações.ValidarTelefone telefone = new Validações.ValidarTelefone();
            if (funcionario.Telefone == string.Empty)
            {
                throw new Exception("O campo 'Telefone' é obrigatório.");
            }

            bool tell = telefone.VerificarTelefone(funcionario.Telefone);
            if (tell == false)
            {
                throw new Exception("Telefone inválido.");
            }

            //NOME

            if (funcionario.Nome == string.Empty)
            {
                throw new Exception("O campo 'Nome' é obrigatório.");
            }

            //CIDADE

            if (funcionario.Cidade == string.Empty)
            {
                throw new Exception("O campo 'Cidade' é obrigatório.");
            }

            //BAIRRO

            if (funcionario.Bairro == string.Empty)
            {
                throw new Exception("O campo 'Bairro' é obrigatório.");
            }

            //RUA

            if (funcionario.Rua == string.Empty)
            {
                throw new Exception("O campo 'Rua' é obrigatório.");
            }

            deptodb.Alterar(funcionario);
        }
        public void Remover(int idcliente)
        {
            FuncionarioDataBase deptodb = new FuncionarioDataBase();
            deptodb.Remover(idcliente);
        }
        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDataBase deptodb = new FuncionarioDataBase();
            List<FuncionarioDTO> depto = deptodb.Listar();
            return depto;
        }

        public List<FuncionarioDTO> Consultar(string nome)
        {
            FuncionarioDataBase database = new FuncionarioDataBase();
            return database.Consultar(nome);
        }
    }
}

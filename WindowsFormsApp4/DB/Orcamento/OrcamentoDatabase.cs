﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;

namespace WindowsFormsApp4.DB.Orcamento
{
    public class OrcamentoDatabase
    {
        public int Salvar(OrcamentoDTO dto)
        {
            string script = @"INSERT INTO tb_orcamento(
                            id_cliente,
                            vl_valor) VALUES(
                            @id_cliente,
                            @vl_valor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.ClienteId));
            parms.Add(new MySqlParameter("vl_valor", dto.Valor));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<OrcamentoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_orcamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<OrcamentoDTO> lista = new List<OrcamentoDTO>();
            while (reader.Read())
            {
                OrcamentoDTO dto = new OrcamentoDTO();
                dto.Id = reader.GetInt32("id_orcamento");
                dto.ClienteId = reader.GetInt32("id_cliente");
                dto.Valor = reader.GetDecimal("vl_valor");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Orcamento
{
    public class OrcamentoBusiness
    {
        public int Salvar(OrcamentoDTO dto)
        {
            OrcamentoDatabase database = new OrcamentoDatabase();

            if (dto.Valor == 0)
            {
                throw new Exception("O valor não pode ser zero");
            }
            return database.Salvar(dto);
        }

        public List<OrcamentoDTO> Listar()
        {
            OrcamentoDatabase database = new OrcamentoDatabase();
            return database.Listar();
        }
    }
}

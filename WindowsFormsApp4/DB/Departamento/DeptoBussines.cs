﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Departamento
{
    public class DeptoBussines
    {
        public int Salvar(DeptoDTO depto)
        {
            DeptoDataBase deptodb = new DeptoDataBase();

            //NOME 

            if (depto.Nome == string.Empty)
            {
                throw new Exception("O campo 'Nome' é obrigatório.");
            }

            //LOCAL

            if (depto.Local == string.Empty)
            {
                throw new Exception("O campo 'Local' é obrigatório.");
            }

            int id = deptodb.Salvar(depto);
            return id;
        }
        public void Alterar(DeptoDTO depto)
        {
            DeptoDataBase deptodb = new DeptoDataBase();
            //NOME 

            if (depto.Nome == string.Empty)
            {
                throw new Exception("O campo 'Nome' é obrigatório.");
            }

            //LOCAL

            if (depto.Local == string.Empty)
            {
                throw new Exception("O campo 'Local' é obrigatório.");
            }


            deptodb.Alterar(depto);
        }
        public void Remover(int idcliente)
        {
            DeptoDataBase deptodb = new DeptoDataBase();
            deptodb.Remover(idcliente);
        }
        public List<DeptoDTO> Listar()
        {
            DeptoDataBase deptodb = new DeptoDataBase();
            List<DeptoDTO> depto = deptodb.Listar();
            return depto;
        }

        public List<DeptoDTO> Consultar(string nome)
        {
            DeptoDataBase database = new DeptoDataBase();
            return database.Consultar(nome);
        }
    }
}

